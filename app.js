//const path = require('path');
//const env_path = path.resolve(__dirname, '.env');
//
//require('dotenv').config({ path: env_path });



const Promise = require('bluebird');
const dbConnection = require('./src/db/Sequelize.js');


//models init
const devicModel = require('./src/db/models/device.model.js');
const deviceMsjModel = require('./src/db/models/deviceMsj.model.js');
const UsuarioModel = require('./src/db/models/usuario.model.js');
const UsuarioDeviceModel = require('./src/db/models/usuarioDevice.model.js');
const tipoBotonModel = require('./src/db/models/tipoBoton.model.js');


const SESHelper = require('./src/helpers/SES.helper.js');
const SNSHelper = require('./src/helpers/SNS.helper.js');


class handlerController {
  constructor() {
        //models
        this.DeviceMsj = deviceMsjModel(dbConnection);
        this.Usuario = UsuarioModel(dbConnection);
        this.UsuarioDevice = UsuarioDeviceModel(dbConnection);
        this.Device = devicModel(dbConnection);
        this.TipoBoton = tipoBotonModel(dbConnection);

        //relations
        this.Usuario.associate(this.Device, this.UsuarioDevice);
        this.Device.associate(this.Usuario, this.UsuarioDevice, this.DeviceMsj,this.TipoBoton);
        this.DeviceMsj.associate(this.Device,this.TipoBoton);
        //this.TipoBoton.associate(this.Device);


        //functions
        this.createParamsDeviceMsjFromEvent = this.createParamsDeviceMsjFromEvent.bind(this);
        this.reportarUsuarios = this.reportarUsuarios.bind(this);
        this.decodeData = this.decodeData.bind(this);
        this.handler = this.handler.bind(this);
  };

  async createParamsDeviceMsjFromEvent(event = {}) {
        const {Device,decodeData,TipoBoton} = this;
        try{
            const device = await Device.findOne({
              where:{ device: event.device},
              include:[{
                required:true,
                model: TipoBoton,
                as: 'tipoBoton'
              }]
            })
            .then((r => (r)? r : {}))
            .catch(err => {console.log(err); return {};});

            const params = {
              id_device: device.id,
              id_tipo_boton: ((device.tipoBoton)? device.tipoBoton.id : 1),
              avgSnr: event.avgSnr,
              data: event.data,
              duplicate: event.duplicate,
              lat: event.lat,
              lng: event.lng,
              rssi: event.rssi,
              seqNumber: event.seqNumber,
              snr: event.snr,
              station: event.station,
              time: toDateTime(event.time),
              mensaje_decodificado: decodeData(event.data),
              payload: (JSON.stringify(event))
            };
            return params;
        }catch(err){
            console.log(`Error createParamsDeviceMsjFromEvent ${err}`);
            return null;
        }
  };

  async reportarUsuarios(resultSaveDeviceMsj,id_device) {
        try {

          const { Usuario,Device  } = this;

          const Subject = 'Mensaje de IOT recibido'

          const usuariosArray = await Usuario.findAll({
              where: [{}],
              include: [{
                model: Device,
                as: 'devicesUsuario',
                required: true,
                where: { id: id_device }
              }]
            })
            .catch(err => { console.log(`Error findAll => ${err}`); return []; });

          const fromEmail = {
            email: process.env.ses_default_from_email,
            name: process.env.ses_default_from_name
          };

          await Promise.each(usuariosArray, async(usuario, i) => {
            try {
              const device = usuario.devicesUsuario;

              console.log(`Device =======> ${JSON.stringify(device)} ============>`);

              const mensaje_not = (device[0])? ((device[0].device_msj_default)? device[0].device_msj_default: 'No msj'): 'No msj';


              const paramsSNS = {
                Message: `${mensaje_not}`,
                PhoneNumber: usuario.telefono,
                Subject: Subject,
              };

              const to = [{
                email: ((usuario.email) ? usuario.email : ''),
                name: ((usuario.nombre) ? usuario.nombre : '')
              }]

              await SESHelper.sendMail(to, Subject,mensaje_not, fromEmail).catch(err => { console.log(`Error sendMail => ${err}`); return null; });
              await SNSHelper.publish(paramsSNS).catch(err => { console.log(`Error publish => ${err}`); return null; });

            } catch (err) {
              console.log(`Error each  ${err}`);
            }
          })

          return true;
        } catch (err) {
          console.log(`Error reportarMensaje ${err}`);
          return null;
        }
  };


  async handler(event) {
        const {createParamsDeviceMsjFromEvent,reportarUsuarios,DeviceMsj} = this;
        const erroMensaje = `No se pudo guadar el deviceMsj`
        console.log(`Evento ${JSON.stringify(event)}`);

        const paramsDeviceMsj = await createParamsDeviceMsjFromEvent(event);

        if(!paramsDeviceMsj){
          return {valido: false,error:erroMensaje};
        }

        const resultSaveDevice = await DeviceMsj.build(paramsDeviceMsj).save().catch(err => { console.log(`Error build => ${err}`); return null; })

        if (!resultSaveDevice) {
           return {valido: false,error:erroMensaje};
        }
        return reportarUsuarios(resultSaveDevice, resultSaveDevice.id_device);
  };

  decodeData(data) {
    return ((data == '00') ? 'Btn tocado' 
                           : ((data == '01') ? 'Btn tocado y baja bateria' : 'Desconocido'));
  };

};

//utilituy

const toDateTime = (secs = 0) => {
  var t = new Date(1970, 0, 1); // Epoch
  t.setSeconds(secs);
  return t;
};


//test

const test = {
  "device": "4D97C0",
  "duplicate": "false",
  "data": "00",
  "time": "1534531855",
  "snr": "21.38",
  "station": "6E88",
  "avgSnr": "24.50",
  "lat": "10.0",
  "lng": "-84.0",
  "rssi": "-122.00",
  "seqNumber": "21"
};


// function testFunc () {
//     const classInstance = new handlerController();
//     return classInstance.handler(test);
// };
// 
// testFunc();


exports.handler = async (event) => {
  try{
    const classInstance = new handlerController();
    return classInstance.handler(event);
  }catch(err){
    return {valido:false,error:err};
  } 

};
