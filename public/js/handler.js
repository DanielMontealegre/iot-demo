let map; // mapa de google
let markers = [];
let dataCharActual = [];
let devicesMensajes = [];

const api = 'http://localhost:8081/' // api
const suffix_get_msj_device = 'iot-msj/';
const suffix_get_msj_device_notocados = 'iot-no-tocados/';
const suffix_get_contadores = 'iot-contadores/';
const iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
const icons = {
    parking: {
        icon: iconBase + 'parking_lot_maps.png'
    },
    library: {
        icon: iconBase + 'library_maps.png'
    },
    info: {
        icon: iconBase + 'info-i_maps.png'
    },
    1:{ // incendio
        icon: api +'assets/images/markers/orange_MarkerI.png'
    },
    2:{ // salto
        icon: api +'assets/images/markers/green_MarkerA.png'
    },
    3:{ //emergenciaMedica
        icon: api +'assets/images/markers/blue_MarkerE.png'
    },
    4:{ //emergenciaMedica
        icon: api +'assets/images/markers/brown_MarkerI.png'
    },
    5:{ //emergenciaMedica
        icon: api +'assets/images/markers/darkgreen_MarkerI.png'
    },
    6:{ //emergenciaMedica
        icon: api +'assets/images/markers/paleblue_MarkerI.png'
    },
    7:{ //emergenciaMedica
        icon: api +'assets/images/markers/purple_MarkerI.png'
    },
    8:{ //emergenciaMedica
        icon: api +'assets/images/markers/pink_MarkerI.png'
    },
    9:{ //emergenciaMedica
        icon: api +'assets/images/markers/paleblue_MarkerI.png'
    },
    10:{ //emergenciaMedica
        icon: api +'assets/images/markers/paleblue_MarkerI.png'
    },
    no_tocado:{
        1:{ // incendio
            icon: api +'assets/images/markers/red_MarkerI.png'
        },
        2:{ // salto
            icon: api +'assets/images/markers/red_MarkerA.png'
        },
        3:{ //emergenciaMedica
            icon: api +'assets/images/markers/red_MarkerE.png'
        },            
        4:{ //emergenciaMedica
            icon: api +'assets/images/markers/red_MarkerI.png'
        },        
        5:{ //emergenciaMedica
            icon: api +'assets/images/markers/red_MarkerI.png'
        },
        6:{ //emergenciaMedica
            icon: api +'assets/images/markers/red_MarkerI.png'
        },
        7:{ //emergenciaMedica
            icon: api +'assets/images/markers/red_MarkerI.png'
        },
        8:{ //emergenciaMedica
            icon: api +'assets/images/markers/red_MarkerI.png'
        },
        9:{ //emergenciaMedica
            icon: api +'assets/images/markers/red_MarkerI.png'
        },
        10:{ //emergenciaMedica
            icon: api +'assets/images/markers/red_MarkerI.png'
        }
    }
};

//handlers

const handleSubmit = () => {
    const filterRadios = $("input[name=filterRadios]:checked").val(); //$('input[name=rbnNumber]:checked').val()
    const checkboxTocados = $(`#checkTocados`);
    const checkBoxNoTocados = $(`#checkNoTocados`);

    if(checkBoxNoTocados[0] && checkBoxNoTocados[0].checked){
        return handleSubmitNoTocados();
    }else{
        const url = new URL(api + suffix_get_msj_device);
        const params = {
            type: filterRadios,
        };
        Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
        return fetch(url)
            .then(r => r.json())
            .then(r => { devicesMensajes = r.data; return r;})
            .then(r => { createMakersFromResponse(r); return r; })
            .catch(err => {
                console.log(err);
                alert('Error!');
            })
    }
}

const handleSubmitNoTocados = () => {
  const filterRadios = $("input[name=filterRadios]:checked").val(); //$('input[name=rbnNumber]:checked').val()
  const url = new URL(api + suffix_get_msj_device_notocados);
  const params = {
    type: filterRadios,
  };
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
  fetch(url)
    .then(r => r.json())
    .then(r => { // format
      r.data = r.data.reduce((result, e, i) => {
        e.device = {
          lat: e.lat,
          lng: e.lng
        };
        e.timeMsj = null;
        e.icon = icons['no_tocado'][e.tipoBoton.id];
        return result.concat(e);
      }, []);
      return r;
    })
    .then(r => { devicesMensajes = r.data; return r;})
    .then(r => { createMakersFromResponse(r); return r; })
    .catch(err => {
      console.log(err);
      alert('Error!');
    })
}




const handleReload = () => {
     const filterRadios = $("input[name=filterRadios]:checked");
     const checkBoxNoTocados = $(`#checkNoTocados`);

     if(filterRadios){
        $(filterRadios).prop('checked', false);
     }
     if(checkBoxNoTocados){
        $(checkBoxNoTocados).prop('checked', false);
     }
    fetch(api + suffix_get_msj_device)
        .then(r => r.json())
        .then(r => { createMakersFromResponse(r); return r; })
        .then(r => {
            dataCharActual = r.data;
            devicesMensajes = r.data;
            initChart();
            return r;
        })
        .then(function(myJson) {
            //console.log(myJson);
        }).catch(err => {
            console.log(err);
            alert('Error!');
        })
}


const handleOnready = () => {

    initContadores();

    fetch(api + suffix_get_msj_device)
        .then(response => response.json())
        .then(response => {
            //console.log('Success:', JSON.stringify(response));
            if (!response.valido) {
                showError(response.error);
                return initMap([]);
            }
            dataCharActual = response.data;
            devicesMensajes = response.data;

            const dispositivos = response.data.reduce((result, e, i) => result.concat(createElementMaker(e)), []);
            initMap(dispositivos);
            initChart();
            return;
        })
        .then(function(myJson) {
            //console.log(myJson);
        }).catch(err => {
            console.log(err);
            alert('Error!');
        })
}

//contadores
const initContadores = () => {
    fetch(api + suffix_get_contadores)
        .then(response => response.json())
        .then(r => {
            const data = r.data;
            loadContadores(data);
        })
        .catch(err => {
            console.log(err);
            alert('Error cargando contadores!');
        })
}

const loadContadores = (data = {}) => {
    const cantidad_dispositivos = $("#cantidad_dispositivos");
    const cantidad_emergencias = $("#cantidad_emergencias");
    const cantidad_incendios = $("#cantidad_incendios");
    const cantidad_asaltos = $("#cantidad_asaltos");

    if (cantidad_dispositivos) cantidad_dispositivos.text((data.dispositivos) ? data.dispositivos : 0); 
    if (cantidad_emergencias) cantidad_emergencias.text((data.emergencias) ? data.emergencias : 0);
    if (cantidad_incendios)  cantidad_incendios.text((data.incendios) ? data.incendios : 0);
    if (cantidad_asaltos) cantidad_asaltos.text((data.asaltos) ? data.asaltos : 0);
    
}


// Init google map
const initMap = (features, lat = 10.0, lng = -84.0) => {
    const element = document.getElementById('map-container');
    if (!element) {
        return;
    }

    map = new google.maps.Map(element, {
        zoom: 8,
        center: new google.maps.LatLng(lat, lng),
        mapTypeId: 'satellite'
    });


    // Create markers.
    features.forEach(function(feature, i) {
        createMaker(feature, map);
    });
}


/// Makers functions 

const createMaker = (feature, gMap) => {

    const marker = new google.maps.Marker({
        position: feature.position,
        icon: ( (feature.icon)? feature.icon.icon : icons[feature.type].icon),
        isNoTocado: ( (feature.icon)? true : false),
        mensaje: feature.mensaje,
        timeMsj: feature.timeMsj,
        id_tipo: feature.id_tipo,
        id_device: feature.id_device,
        draggable: false,
        animation: google.maps.Animation.DROP,
        map: gMap
    });
    marker.addListener('click', function(e) {
        alert(`Reporte: ${cantidadTocada(this)}`);
    });

    setTimeout(function() {
          marker.setMap(gMap);
    }, 500);
    
    markers.push(marker);
}

const cantidadTocada = (maker) => {
    if(maker.isNoTocado){
        return 0;
    }else{
       return devicesMensajes.reduce((r,e,i) => 
        (e.id_tipo_boton == maker.id_tipo && maker.id_device == e.id_device)? 
          r + e.tipoBoton.titulo  +' '+dateFormat(e.time) + '\n'
        :  
         r
        ,'\n');
    }
}

const createMakersFromResponse = (response = {data:[]}) => {
    //Delete and Create markers.
    deleteMarkers();
    if (!response.valido) {
        showError(response.error);
        return;
    }


    const dispositivos = response.data.reduce((result, e, i) => result.concat(createElementMaker(e)), []);
    dispositivos.forEach(function(feature, i) {
        createMaker(feature, map);
    });
};

const createElementMaker = (deviceMsj) => {
    return {
        position: new google.maps.LatLng(deviceMsj.device.lat, deviceMsj.device.lng),
        tipoBoton: deviceMsj.tipoBoton.id,
        type: deviceMsj.tipoBoton.id,
        id_tipo: deviceMsj.tipoBoton.id,
        id_device: deviceMsj.id_device,
        icon:deviceMsj.icon,
        mensaje: deviceMsj.tipoBoton.titulo,
        timeMsj: deviceMsj.time
    }
}

// Sets the map on all markers in the array.
const setMapOnAll = (gMap) => {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(gMap);
    }
}

// Removes the markers from the map, but keeps them in the array.
const clearMarkers = () => {

    setMapOnAll(null);
}

// Shows any markers currently in the array.
const showMarkers = () => {

    setMapOnAll(map);
}

// Deletes all markers in the array by removing references to them.
const deleteMarkers = () => {
    clearMarkers();
    markers = [];
}


const showError = (error) => {
    alert(error);
}

/////chart functions

const dataCharAgrupadaEmergencias = (deviceMsj) => {
    const obj = deviceMsj.reduce((result, e, i) => {
        const titulo = e.tipoBoton.titulo;
        if (result[titulo]) {
            result[titulo] += 1;
        } else {
            result[titulo] = 1;
        }
        return result;
    }, {})

    let array = [];

    for (key in obj) {
        array.push([key, obj[key]])
    }
    return array;
}

const dataChartEmergencias = (deviceMsj) => {
    try {
        let meses = [];
        let charData = deviceMsj.reduce((result, e, i) => {
            const fecha = new Date(e.time);
            const mes = fecha.getMonth() + 1;
            const id_tipo = e.tipoBoton.id;
            const titulo = e.tipoBoton.titulo;
            if (result[id_tipo]) {
                if (result[id_tipo][mes]) {
                    result[id_tipo][mes] += 1;
                } else {
                    result[id_tipo][mes] = 1;
                }
            } else {
                result[id_tipo] = {
                    [mes]: 1,
                    nombre: titulo
                }
            }
            if (!meses.some((m, i) => m == mes)) {
                meses = meses.concat(mes);
            }
            return result;
        }, {});

        charData = fillMonths(charData, meses);

        let charDataFormatNombres = ['Mes'];
        let charDataValorMes = {};
        let valores = [];

        for (let key in charData) {
            const tipo = charData[key];
            charDataFormatNombres = charDataFormatNombres.concat(tipo.nombre);
            for (key02 in tipo) {
                if (key02 != 'nombre') {
                    if (!Array.isArray(charDataValorMes[monthFromInt(key02)])) { //!charDataValorMes[monthFromInt(key02)].some((e,i) => (e == i))
                        charDataValorMes[monthFromInt(key02)] = [monthFromInt(key02), tipo[key02]];
                    } else {
                        charDataValorMes[monthFromInt(key02)] = charDataValorMes[monthFromInt(key02)].concat([tipo[key02]]);
                    }

                }
            }
        }

        valores.push(charDataFormatNombres);
        for (let key in charDataValorMes) {
            valores.push(charDataValorMes[key]);
        }
        return valores;
    } catch (err) {
        console.log(err);
        return [];
    }
}

const fillMonths = (charData, meses) => {
    let charDataAux = {};
    for (emergencia in charData) {
        const obj = charData[emergencia];
        const mesesKeys = Object.keys(obj);
        charDataAux[emergencia] = meses.reduce((result, mes, i) => {
            if (!result.hasOwnProperty(mes)) {
                result[mes] = 0
                return result;
            } else {
                return result;
            }
        }, obj);
    }
    return charDataAux;
}

const monthFromInt = (mes) => {
    if (mes == 1) {
        return 'Enero';
    }
    if (mes == 2) {
        return 'Febrero';
    }
    if (mes == 3) {
        return 'Marzo';
    }
    if (mes == 4) {
        return 'Abril';
    }
    if (mes == 5) {
        return 'Mayo';
    }
    if (mes == 6) {
        return 'Junio';
    }
    if (mes == 7) {
        return 'Julio';
    }
    if (mes == 8) {
        return 'Agosto';
    }
    if (mes == 9) {
        return 'Septiembre';
    }
    if (mes == 10) {
        return 'Octubre';
    }
    if (mes == 11) {
        return 'Noviembre';
    }
    if (mes == 12) {
        return 'Deciembre';
    }
}

const initChart = (dataNew) => {
    // google.load('current', { 'packages': ['corechart'] });
    setTimeout(function() {
        google.charts.load('current', { 'packages': ['corechart'] });
        google.charts.setOnLoadCallback(drawChart);
        google.charts.setOnLoadCallback(drawChart2);
    }, 200);

}

const drawChart = (dataNew) => {
    const element = document.getElementById('chart_div');
    if (!element) {
        return;
    }
    const valores = dataChartEmergencias(dataCharActual);
    const data = google.visualization.arrayToDataTable(valores);
    const options = {
        title: 'Grafico de emergencias emitidas',
        titleTextStyle,
        fontName: 'sans-serif',
        fontSize: 14,
        colors,
        color: '#308ee0',
        hAxis: { title: 'Mes', titleTextStyle },
        vAxis: { minValue: 0, titleTextStyle },
        annotations
    };

    const chart = new google.visualization.AreaChart(element);
    chart.draw(data, options);
}

function drawChart2() {
    const element = document.getElementById('piechart');
    if (!element) {
        return;
    }
    const arrayData = dataCharAgrupadaEmergencias(dataCharActual);

    let init = [
        ['Emergencias', 'Cantidad']
    ];

    arrayData.forEach((e, i) => init.push(e));



    var data = google.visualization.arrayToDataTable(init);

    var options = {
        title: 'Usos del boton de emergencia',
        fontName: '"Poppins", sans-serif',
        fontSize: 14,
        titleTextStyle,
        color: '#308ee0',
        is3D: true,
        colors,
        vAxis: { titleTextStyle },
        hAxis: { titleTextStyle },
        annotations
    };

    var chart = new google.visualization.PieChart(element);

    chart.draw(data, options);
}


const titleTextStyle = { color: '#308ee0', fontName: '"Poppins", sans-serif', italic: false, fontSize: 14, bold: true }

const colors = ['#ffce56', '#ff6384', '#36a2eb', '#4bc075', '#ff9f40'];
const annotations = {
    textStyle: {
        fontName: '"Poppins", sans-serif',
        fontSize: 14,
        bold: false,
        italic: false,
        fontWeight: 500,
        // The color of the text.
        color: '#308ee0',
        // The color of the text outline.
        auraColor: '#d799ae',
        // The transparency of the text.
        opacity: 0.8
    }
}