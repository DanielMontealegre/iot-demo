    const dateFormat = (d = new Date()) => {
      if(d == null || d == '' || !d){
        return '';
      }
      d = new Date(d);
      dformat = [d.getMonth() + 1,
        d.getDate(),
        d.getFullYear()
      ].join('-') + ' ' + [d.getHours(),
        d.getMinutes(),
        d.getSeconds()
      ].join(':');
      return dformat;
    }



