const path = require('path');
const env_path = path.resolve(__dirname, '.env');

require('dotenv').config({ path: env_path });

const Sequelize = require('sequelize');
const moment = require('moment');
const express = require('express');
const app = express();
const PORT = 8081


const dbConnection = require('./src/db/Sequelize.js');
const deviceMsjModel = require('./src/db/models/deviceMsj.model.js');
const deviceModel = require('./src/db/models/device.model.js');
const tipoBotonModel = require('./src/db/models/tipoBoton.model.js');




app.use(express.static('public'));


app.listen(PORT, function() {
  console.log('App running on port ' + PORT);
});


app.get('/iot-msj', (req, res) => {
  const DeviceMsj = deviceMsjModel(dbConnection);
  const TipoBoton = tipoBotonModel(dbConnection);
  const Device = deviceModel(dbConnection);

  DeviceMsj.associate(Device,TipoBoton);


  const type = req.query.type;

  const where = createWhere(type);

  return DeviceMsj.findAll({
    subQuery:false,
    where,
    limit: 50,
    offset: 0,
    attributes: {
      exclude: ['payload']
    },
    include:[{
      model: TipoBoton,
      as: 'tipoBoton',
      required:true
    },{
      model: Device,
      as: 'device',
      attributes:['lat','lng','device_msj_default'],
      required:true
    }],
  }).then(r => {
    res.status(200);
    res.json({valido:true,data: r});
    return r;
  }).catch(err => {
    console.log(err);
    res.status(200);
    res.json({
      valido: false,
      error: err
    });
    return null;
  })

});

app.get('/iot-contadores', async(req, res) => {
    try {

        const DeviceMsj = deviceMsjModel(dbConnection);
        const Device = deviceModel(dbConnection);


        const tipo = {
            incendios: 1,
            asaltos: 2,
            emergencias: 3
        }


        const type = req.query.type;
        const tocados = req.query.tocados;
        const notocados = req.query.notocados;

        const dispositivos = await Device.count({ where: { device: {$ne: null }} }).catch(error => { console.log(error); return 0; })
        const emergencias = await DeviceMsj.count({ where: { id_tipo_boton: tipo.emergencias } }).catch(error => { console.log(error); return 0; })
        const incendios = await DeviceMsj.count({ where: { id_tipo_boton: tipo.incendios } }).catch(error => { console.log(error); return 0; })
        const asaltos = await DeviceMsj.count({ where: { id_tipo_boton: tipo.asaltos } }).catch(error => { console.log(error); return 0; })

        const r = {
            dispositivos,
            emergencias,
            incendios,
            asaltos
        }
        res.status(200);
        res.json({valido:true,data: r});
        return r;

    } catch (err) {
        console.log(err);
        res.status(200);
        res.json({
            valido: false,
            error: err
        });
    }
});

app.get('/iot-no-tocados',async(req,res) => {
  try{
     const DeviceMsj = deviceMsjModel(dbConnection);
     const Device = deviceModel(dbConnection);
     const TipoBoton = tipoBotonModel(dbConnection);
     
     const type = req.query.type;
     const where = createWhere(type);

     Device.associate(null,null,DeviceMsj,TipoBoton);

    const result = await Device.findAll({
      subQuery:false,
      include:[{
        model: DeviceMsj,
        as:'diviceMsjs',
        required:false,
        where:where
      },{
        model: TipoBoton,
        as:'tipoBoton',
        required:true 
      }]
    }).catch(err => { console.log(err); return []; })  

    const finalResult = result.reduce((r,e,i) => (e.diviceMsjs == 0 || !e.diviceMsjs)? r.concat(e): r,[]);

    res.status(200);
    res.json({valido:true,data: finalResult});
    return r;   

  }catch(err){
    if(res.headersSent) return;
        console.log(err);
        res.status(200);
        res.json({
            valido: false,
            error: err
        });
        return;
  }

})

const createWhere = (type = null) => {

  if (type === null || typeof type == 'undefined') {
    return {};
  }
  const where = {};
  const hoy = moment();

  switch (true) {
    case 1 == type: // hoy
      where.time = diaExactoWhere('time',hoy);
      break;
    case 2 == type: // ayer
      hoy.subtract(1, 'days');
      where.time = diaExactoWhere('time',hoy);
      break;
    case 3 == type: // esta semana
      const starOfWeek = hoy.clone();
      starOfWeek.startOf('week');
      where.time = betweenFechasWhere(hoy,starOfWeek);
      break;
    case 4 == type: // este mes
      const starOfMonth = hoy.clone();
       starOfMonth.startOf('month');
      where.time = betweenFechasWhere(hoy,starOfMonth);
      break;
    case 5 == type: // ultimos 3 meses
      const ultimosTresMeses = hoy.clone();
      ultimosTresMeses.subtract(3, 'month');
      ultimosTresMeses.startOf('month');
      where.time = betweenFechasWhere(hoy,ultimosTresMeses);
      break;
    default:
      break
  }
  return where;
}

const diaExactoWhere = (colName = 'time',date = moment()) => {
    return  {
        $and: [
          Sequelize.where(Sequelize.fn('YEAR', Sequelize.col(colName)), date.format('Y')),
          Sequelize.where(Sequelize.fn('MONTH', Sequelize.col(colName)), date.format('M')),
          Sequelize.where(Sequelize.fn('DAY', Sequelize.col(colName)), date.format('D'))
        ]
      }
}

const betweenFechasWhere = (dateEnd = moment(),dateBegin = moment()) => {
    return {
            $gte: dateBegin.toDate(),
            $lte: dateEnd.toDate()
    }
}