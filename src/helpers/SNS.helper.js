
const AWS = require('aws-sdk');
const SNS = new AWS.SNS({});



/*
 * Función para realizar un push en SNS AWS
 * parametros:
 *  deviceId-> STRING
 *  platform-> STRING
 *  lang-> STRING
 *  callBack-> FUNCTION
 */
module.exports.publish = (params) => {
    return SNS.publish(params).promise()
    .then(data => {
            console.log('SNS done! \n' + JSON.stringify(data));
            return true;
     })
    .catch(err => {
        console.log('Error on SNS' + err);
        return false
    });
};

