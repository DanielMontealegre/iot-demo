//Variables
const AWS = require('aws-sdk');
const SES = new AWS.SES({ });
/*
 * Función para realizar envíos por correo
 * parametros:
 *  to-> Array of objects: [{email: ''{, name: ''}}, {email: ''{, name: ''}}, ...]
 *  subject-> Text
 *  body-> HTML Text
 *  from-> Object: {email: ''{, name: ''}}
 */
module.exports.sendMail = (to, subject, body = '', from) => {
    if (!from) {
        from = {
                email: process.env.ses_default_from_email,
                name: process.env.ses_default_from_name
        }
    }

    let message_params = {
        Destination: { ToAddresses: getEmailsString(to) },
        Message: {
            Body: { Html: { Data: body } },
            Subject: { Data: subject }
        },
        Source: emailObjectToString(from)
    };


    const promise = SES.sendEmail(message_params).promise();

    return promise.then(data => {
            console.log('Email send! \n' + JSON.stringify(data));
            return true;
        }).catch(err => {
            console.log('Email not send! \n' + err);
            return false;
        })

};

function getEmailsString(objects) {
    let data = [];
    objects.forEach( (object) =>  {
        data.push(emailObjectToString(object));
    });
    return data;
}

/* Formato Nombre <Email> */
function emailObjectToString(object) {
    if (object.name && object.name !== '') {
        return '"' + object.name + '" <' + object.email + '>';
    } else {
        return object.email;
    }
}