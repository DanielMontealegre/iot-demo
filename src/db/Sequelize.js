
const Sequelize = require('sequelize');


const env = process.env;


const ConfigDataBaseMaster = {
  database: env.db_name,
  username: env.dbuser,
  password: env.dbpassword,
  port: env.port,
  timezone_sequelize:  env.timezone_sequelize,
  host_write: env.dbhost_write,
  host_read: env.dbhost_read
}



const sequelizeMaster = new Sequelize(ConfigDataBaseMaster.database, ConfigDataBaseMaster.username, ConfigDataBaseMaster.password, {
  dialect: 'mysql',
  timezone: ConfigDataBaseMaster.timezone_sequelize,
  port: ConfigDataBaseMaster.port,
  replication: {
    write: {
      host: ConfigDataBaseMaster.host_write,
      pool: {
        min: 1,
        max: 75,
        idle: 8000
      }
    },
    read: [{
      host: ConfigDataBaseMaster.host_read,
      pool: {
        min: 1,
        max: 75,
        idle: 8000
      }
    }]
  }
});

module.exports = sequelizeMaster;
