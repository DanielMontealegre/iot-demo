const sequelize = require('sequelize');

const getFunc = (Sequelize, tableName = 'tipo_boton') => {
    const tipoBoton = Sequelize.define('tipoBoton', {
        id: {
            type: sequelize.INTEGER,
            primaryKey: true
        },
        titulo: {
            type: sequelize.STRING,
            primaryKey: true
        },
        descripcion: {
            type: sequelize.STRING
        }

    }, {
        // don't add the timestamp attributes (updatedAt, createdAt)
        timestamps: false,

        // don't delete database entries but set the newly added attribute deletedAt
        // to the current date (when deletion was done). paranoid will only work if
        // timestamps are enabled
        //paranoid: true,

        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,
        // disable the modification of table names; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        freezeTableName: true,
        // define the table's name
        tableName
    });
    //Model relations
    tipoBoton.associate = () => {

        
    };

    //Simplify raw queries
    tipoBoton.query = (query, spread) => {
        if (spread) {
            return sequelize.query(query);
        } else {
            return sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
        }
    };
    return tipoBoton;
};


module.exports = getFunc;