const sequelize = require('sequelize');

const getFunc = (Sequelize, tableName = 'usuario') => {
    const Usuario = Sequelize.define('usuario', {
        id: {
            type: sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        nombre: {
            type: sequelize.STRING,
            allowNull: false
        },
        email: {
            type: sequelize.STRING,
            unique: true
        },
        telefono: {
            type: sequelize.STRING
        },
        direccion_exacta: {
            type: sequelize.TEXT
        },
        mensaje: {
            type: sequelize.TEXT
        },
        updated_at: {
            type: sequelize.DATE
        },
        created_at: {
            type: sequelize.DATE
        }

    }, {
        // don't add the timestamp attributes (updatedAt, createdAt)
        timestamps: true,

        // don't delete database entries but set the newly added attribute deletedAt
        // to the current date (when deletion was done). paranoid will only work if
        // timestamps are enabled
        //paranoid: true,

        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,
        // disable the modification of table names; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        freezeTableName: true,
        // define the table's name
        tableName
    });
    //Model relations
    Usuario.associate = (Device,UsuarioDevice) => {
        if (Device && UsuarioDevice) {
            Usuario.belongsToMany(Device, { through: UsuarioDevice, as: 'devicesUsuario', foreignKey: 'id_usuario', targetKey: 'id_usuario' });
        }
    };

    //Simplify raw queries
    Usuario.query = (query, spread) => {
        if (spread) {
            return sequelize.query(query);
        } else {
            return sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
        }
    };
    return Usuario;
};


module.exports = getFunc;