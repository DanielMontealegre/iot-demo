const sequelize = require('sequelize');

const getFunc = (Sequelize, tableName = 'device') => {
    const Device = Sequelize.define('device', {
        id: {
            type: sequelize.INTEGER,
            primaryKey: true
        },
        device: {
            type: sequelize.STRING,
            allowNull:false,
            unique: true
        },
        id_tipo_boton:{
            type: sequelize.INTEGER
        },
        device_msj_default:{
            type: sequelize.STRING
        },
        lat:{
            type: sequelize.STRING
        },
        lng:{
            type: sequelize.STRING
        },
        updated_at: {
            type: sequelize.DATE
        },
        created_at: {
            type: sequelize.DATE
        }

    }, {
        // don't add the timestamp attributes (updatedAt, createdAt)
        timestamps: true,

        // don't delete database entries but set the newly added attribute deletedAt
        // to the current date (when deletion was done). paranoid will only work if
        // timestamps are enabled
        //paranoid: true,

        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,
        // disable the modification of table names; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        freezeTableName: true,
        // define the table's name
        tableName
    });
    //Model relations
    Device.associate = (Usuario,UsuarioDevice,DeviceMsj,TipoBoton) => {
        if (Usuario && UsuarioDevice) {
            Device.belongsToMany(Usuario, { through :UsuarioDevice, as: 'devicesUsuario', foreignKey: 'id_device', targetKey: 'id_device' });
        }
       if(DeviceMsj){
            Device.hasMany(DeviceMsj, {  as: 'diviceMsjs', foreignKey: 'id_device', targetKey: 'id' });
        }
        if(TipoBoton){
            Device.belongsTo(TipoBoton, {  as: 'tipoBoton', foreignKey: 'id_tipo_boton', targetKey: 'id' });
        }
    };

    //Simplify raw queries
    Device.query = (query, spread) => {
        if (spread) {
            return sequelize.query(query);
        } else {
            return sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
        }
    };
    return Device;
};


module.exports = getFunc;