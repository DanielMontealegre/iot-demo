const sequelize = require('sequelize');


const getFunc = (Sequelize, tableName = 'device_msj') => {
    const DeviceMsj = Sequelize.define('deviceMsj', {
        id: {
            type: sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        id_device: {
            type: sequelize.INTEGER,
            allowNull: false
        },
        id_tipo_boton: {
            type: sequelize.INTEGER,
            allowNull: false
        },
        avgSnr: {
            type: sequelize.STRING,
        },
        data: {
            type: sequelize.STRING,
        },
        duplicate: {
            type: sequelize.STRING,
        },
        rssi: {
            type: sequelize.STRING,
        },
        time: {
            type: sequelize.DATE
        },
        lat: {
            type: sequelize.STRING
        },
        lng: {
            type: sequelize.STRING
        },
        seqNumber: {
            type: sequelize.STRING
        },
        snr: {
            type: sequelize.STRING
        },
        station: {
            type: sequelize.STRING
        },
        mensaje_decodificado: {
            type: sequelize.TEXT
        },
        payload: {
            type: sequelize.TEXT
        },
        updated_at: {
            type: sequelize.DATE
        },
        created_at: {
            type: sequelize.DATE
        }

    }, {
        // don't add the timestamp attributes (updatedAt, createdAt)
        timestamps: true,

        // don't delete database entries but set the newly added attribute deletedAt
        // to the current date (when deletion was done). paranoid will only work if
        // timestamps are enabled
        //paranoid: true,

        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,
        // disable the modification of table names; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        freezeTableName: true,
        // define the table's name
        tableName
    });
    //Model relations
    DeviceMsj.associate = (Device,TipoBoton) => {
        if(Device){
           DeviceMsj.belongsTo(Device, {  as: 'device', foreignKey: 'id_device', targetKey: 'id' });
        }
        if(TipoBoton){
           DeviceMsj.belongsTo(TipoBoton, {  as: 'tipoBoton', foreignKey: 'id_tipo_boton', targetKey: 'id' });
        }
    };

    //Simplify raw queries
    DeviceMsj.query = (query, spread) => {
        if (spread) {
            return sequelize.query(query);
        } else {
            return sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
        }
    };
    return DeviceMsj;
};


module.exports = getFunc;