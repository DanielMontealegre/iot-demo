const sequelize = require('sequelize');

const getFunc = (Sequelize, tableName = 'device_usuario') => {
    const UsuarioDevice = Sequelize.define('usuarioDevice', {
        id_usuario: {
            type: sequelize.INTEGER,
            primaryKey: true
        },
        id_device: {
            type: sequelize.STRING,
            primaryKey: true
        },
        updated_at: {
            type: sequelize.DATE
        },
        created_at: {
            type: sequelize.DATE
        }

    }, {
        // don't add the timestamp attributes (updatedAt, createdAt)
        timestamps: true,

        // don't delete database entries but set the newly added attribute deletedAt
        // to the current date (when deletion was done). paranoid will only work if
        // timestamps are enabled
        //paranoid: true,

        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,
        // disable the modification of table names; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        freezeTableName: true,
        // define the table's name
        tableName
    });
    //Model relations
    UsuarioDevice.associate = () => {

        
    };

    //Simplify raw queries
    UsuarioDevice.query = (query, spread) => {
        if (spread) {
            return sequelize.query(query);
        } else {
            return sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
        }
    };
    return UsuarioDevice;
};


module.exports = getFunc;